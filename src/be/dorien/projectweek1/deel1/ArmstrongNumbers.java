package be.dorien.projectweek1.deel1;

import java.util.Scanner;
import java.lang.Math;

public class ArmstrongNumbers {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a three-digit integer: ");
        int getal = keyboard.nextInt();
        String getalAlsString = String.valueOf(getal);
        char[] cijfers = getalAlsString.toCharArray();

        double sum = 0;
        for(int i = 0; i < cijfers.length; i++){
            int cijfer = Character.getNumericValue(cijfers[i]);
            sum += Math.pow(cijfer, cijfers.length);
        }

        if(getal == sum){
            System.out.println(getal + " is an Armstrong number.");
        }else{
            System.out.println(getal + " is not an Armstrong number.");
        }
    }
}
