package be.dorien.projectweek1.deel1;

public class ComputerAlphabet {
    public static void main(String[] args) {
        for(int i = 'A'; i <= 'z'; i++){
            if(!(91 <= i && i <= 96)){
            char character = (char) i;
            String binary = Integer.toBinaryString(i);
            String hexa = Integer.toHexString(i);
            System.out.println(character + " : " + i + " : " + binary + " : " + hexa);
            }
        }
    }
}
