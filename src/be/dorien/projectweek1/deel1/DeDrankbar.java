package be.dorien.projectweek1.deel1;

import java.util.Scanner;

public class DeDrankbar {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Welkom in de drank-bar!");
        System.out.print("Wat is uw naam? : ");
        String naam = keyboard.nextLine();

        String[] menu = {"Water", "Cola", "Koffie", "Bier", "Ik sla een keer over", "De rekening alstublief!"};
        int gekozenDrankje;
        int aantalDrankjes = 0;

        do {
            getMenu(menu);
            System.out.print("Heeft u een keuze kunnen maken? (geef het getal): ");
            gekozenDrankje = keyboard.nextInt();

            if(gekozenDrankje == 6){
                System.out.println("U heeft in totaal " + aantalDrankjes + " drankjes besteld.");
                System.out.println("Bedankt voor uw bezoek!");
            }else if(gekozenDrankje == 5) {
                System.out.println("Geen probleem!");
            }else if(gekozenDrankje == 4){
                    System.out.print("Wat is uw leeftijd? : ");
                    int leeftijd = keyboard.nextInt();
                    if(leeftijd < 18){
                        System.out.println(naam + ", u bent nog te jong voor een biertje!");
                    }else{
                        aantalDrankjes++;
                        System.out.println("Hier is uw " + menu[gekozenDrankje-1] + ", " + naam + ". Smakelijk!");
                    }
            }else{
                aantalDrankjes++;
                System.out.println("Hier is uw " + menu[gekozenDrankje-1] + ", " + naam + ". Smakelijk!");
            }
        } while (gekozenDrankje != 6);
    }//einde main

    static void getMenu(String[] menu){
        for(int i = 0; i < menu.length; i++){
            System.out.println(i+1 + ". " + menu[i]);
        }
    }
}
