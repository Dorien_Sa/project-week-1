package be.dorien.projectweek1.deel1;

import java.util.Scanner;

public class TussenTeller {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int getal1 = keyboard.nextInt();
        System.out.print("Enter a second number: ");
        int getal2 = keyboard.nextInt();

        int sum = 0;
        int product = 1;

        for(int i = getal1 +1 ; i < getal2; i++){
            sum += i;
            product *= i;
        }
        System.out.println("De som is : " + sum);
        System.out.println("Het product is : " + product);
    }
}
