package be.dorien.projectweek1.deel1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UnitConverter {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        String[] conversies = {"Euro --> Dollar", "Celsius --> Fahrenheit", "Meters --> Feet", "Kilometers --> Miles", "Afsluiten"};
        int keuze;
        do {

            getConversies(conversies);
            System.out.print("Welke conversie wilt u uitvoeren?: ");
            try{
                keuze = keyboard.nextInt();
            }catch (InputMismatchException ime){
                System.out.println("De ingevulde waarde moet een getal zijn.");
                System.out.println(ime.getMessage());
                keyboard.nextLine();
                keuze = 0;
                continue;
            }

            switch (keuze){
                case 1:
                    System.out.print("Geef waarde in euro: ");
                    double euro = keyboard.nextDouble();
                    double dollar = euro * 1.22;
                    System.out.println("De waarde in dollar is: " + dollar);
                    break;
                case 2:
                    System.out.print("Geef temperatuur in celsius: ");
                    double celsius = keyboard.nextDouble();
                    double fahrenheit = celsius * (9.0/5) + 32;
                    System.out.println("De temperatuur in fahrenheit is: " + fahrenheit);
                    break;
                case 3:
                    System.out.print("Geef afstand in meter: ");
                    double meter = keyboard.nextDouble();
                    double feet = meter * 3.28;
                    System.out.println("De afstand in feet is: " + feet);
                    break;
                case 4:
                    System.out.print("Geef afstand in kilometers: ");
                    double kilometer = keyboard.nextDouble();
                    double miles = kilometer * 0.62137;
                    System.out.println("De afstand in mijlen is: " + miles);
                    break;
                case 5:
                    System.out.println("Tot de volgende!");
                    break;
                default:
                    System.out.println("Gelieve een correcte waarde in te geven.");
            }
        } while(keuze != 5);

//        System.out.print("Geef afstand in kilometers: ");
//        double kilometer = keyboard.nextDouble();
//        double miles = kilometer * 0.62137;
//        System.out.println("De afstand in mijlen is: " + miles);
    }

    static void getConversies(String[] conversies){
        for(int i = 0; i < conversies.length; i++){
            System.out.println(i+1 + ". " + conversies[i]);
        }
    }
}
