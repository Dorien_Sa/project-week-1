package be.dorien.projectweek1.deel2;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        System.out.println("Welkom in onze main app!");

        int gekozenOptie;

        do{
            toonMenu();

            Scanner keyboard = new Scanner(System.in);
            gekozenOptie = keyboard.nextInt();

            switch (gekozenOptie){
                case 1:
                    fibonacci();
                    break;
                case 2:
                    somEvenFabonicciGetallen();
                    break;
                case 3:
                    isEenPriemgetal();
                    break;
                case 4:
                    volgendePriemgetal();
                    break;
                case 5:
                    eerstePriemgetalTussenTweeWaarden();
                    break;
                case 6:
                    isPalindroomString();
                    break;
                case 7:
                    isPalindroomGetal();
                    break;
                case 8:
                    volgendePalindroomGetal();
                    break;
                case 9:
                    eerstePalindroomTussenTweeWaarden();
                    break;
                case 10:
                    maakPalindroom();
                    break;
                case 11:
                    System.out.println("Tot de volgende keer!");
                default:
                    System.out.println("Gelieve een correcte waarde in te geven.");
            }
        } while(gekozenOptie != 11);
    }//einde main

    public static void fibonacci(){
        int waarde = 0;
        int vorigeWaarde = 0;
        int tweedeVorigeWaarde = 0;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Kies een maximum waarde: ");
        int max = keyboard.nextInt();

        for(int i = 0; waarde < max; i++){
            if(i == 0){
                waarde = 1;
                System.out.println(waarde + " ");
                vorigeWaarde = waarde;
            }else if(i == 1){
                waarde = 1;
                System.out.println(waarde + " ");
                tweedeVorigeWaarde = vorigeWaarde;
                vorigeWaarde = waarde;
            }else{
                waarde = vorigeWaarde + tweedeVorigeWaarde;
                System.out.println(waarde + " ");
                tweedeVorigeWaarde = vorigeWaarde;
                vorigeWaarde = waarde;
            }
        }
    }//einde fibonacci

    public static void somEvenFabonicciGetallen(){
        int sum = 0;
        int waarde = 0;
        int vorigeWaarde = 0;
        int tweedeVorigeWaarde = 0;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Kies een maximum waarde: ");
        int max = keyboard.nextInt();

        for(int i = 0; waarde < max; i++){
            if(i == 0){
                waarde = 1;
                vorigeWaarde = waarde;
            }else if(i == 1){
                waarde = 1;
                tweedeVorigeWaarde = vorigeWaarde;
                vorigeWaarde = waarde;
            }else{
                waarde = vorigeWaarde + tweedeVorigeWaarde;
                if(waarde % 2 == 0){
                    sum += waarde;
                }
                tweedeVorigeWaarde = vorigeWaarde;
                vorigeWaarde = waarde;
            }
        }
        System.out.println("De som is gelijk aan: " + sum);
    }//einde som fibonacci

    public static void isEenPriemgetal(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef een getal in: ");
        int getal = keyboard.nextInt();
        for(int i = 2; i < getal; i++){
            if(getal % i == 0){
                //geen priemgetal
                System.out.println(getal + " is geen priemgetal.");
                break;
            }else if(i == getal-1){
                //getal is een priemgetal
                System.out.println(getal + " is een priemgetal.");
            }
        }
    }//einde is en priemgetal

    public static void volgendePriemgetal(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef een getal in: ");
        boolean priemgetalGevonden = false;
        int getal = keyboard.nextInt();
        for(int i = getal+1; i < Integer.MAX_VALUE; i++){
            for(int j = 2; j < i; j++){
                if(i % j == 0){
                    //geen priemgetal
                    break;
                }else if(j == i-1){
                    //getal is een priemgetal
                    System.out.println(i + " is het volgende priemgetal.");
                    priemgetalGevonden = true;
                    break;
                }
            }
            if(priemgetalGevonden){
                break;
            }

        }
    }//einde volgende priemgetal

    public static void eerstePriemgetalTussenTweeWaarden(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef een eerste getal in: ");
        int getal1 = keyboard.nextInt();
        System.out.print("Geef een tweede getal in: ");
        int getal2 = keyboard.nextInt();

        boolean priemgetalGevonden = false;

        for(int i = getal1+1; i < getal2; i++){
            for(int j = 2; j < i; j ++){
                if(i % j == 0){
                    //geen priemgetal
                    break;
                }else if(j == i-1){
                    //priemgetal
                    System.out.println(i + " is het eerste priemgetal tussen " + getal1 + " en " + getal2);
                    priemgetalGevonden = true;
                    break;
                }
            }
            if(priemgetalGevonden){
                break;
            }
        }
        if(!priemgetalGevonden){
            System.out.println("Er zijn geen priemgetallen gevonden tussen " + getal1 + " en " + getal2);
        }
    }//einde eerste priemgetal tussen twee waarden

    public static void isPalindroomString(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef uw tekst in: ");
        String tekst = keyboard.nextLine().toLowerCase();
        String omgekeerdeTekst = "";
        for(int i = tekst.length()-1; i >= 0; i--){
            omgekeerdeTekst += tekst.charAt(i);
        }

        if(tekst.equals(omgekeerdeTekst)){
            System.out.println(tekst + " is een palindroom.");
        }else{
            System.out.println(tekst + " is geen palindroom");
        }

    }//einde is palindroom string

    public static void isPalindroomGetal(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef een getal in: ");
        int getal = keyboard.nextInt();

        StringBuilder getalString = new StringBuilder();
        getalString.append(getal);

        StringBuilder omgekeerdGetalString = new StringBuilder();
        omgekeerdGetalString.append(getal).reverse();

        if(getalString.toString().equals(omgekeerdGetalString.toString())){
            System.out.println(getal + " is een palindroom.");
        }else{
            System.out.println(getal + " is geen palindroom");
        }
    }//einde is palindroom getal

    public static void volgendePalindroomGetal(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef een getal in: ");
        int getal = keyboard.nextInt();
        StringBuilder getalString = new StringBuilder();
        StringBuilder omgekeerdGetalString = new StringBuilder();

        for(int i = getal+1; i < Integer.MAX_VALUE; i++){
            getalString.append(i);
            omgekeerdGetalString.append(i).reverse();

            if(getalString.toString().equals(omgekeerdGetalString.toString())){
                System.out.println(i + " is het eerst volgende palindroom.");
                break;
            }else{
                getalString.delete(0,Integer.MAX_VALUE);
                omgekeerdGetalString.delete(0,Integer.MAX_VALUE);
            }
        }
    }//einde volgende palindroom getal

    public static void eerstePalindroomTussenTweeWaarden(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef een eerste getal in: ");
        int getal1 = keyboard.nextInt();
        System.out.print("Geef een tweede getal in: ");
        int getal2 = keyboard.nextInt();

        StringBuilder getalString = new StringBuilder();
        StringBuilder omgekeerdGetalString = new StringBuilder();

        boolean palindroomGevonden = false;

        for(int i = getal1+1; i < getal2; i++){
            getalString.append(i);
            omgekeerdGetalString.append(i).reverse();

            if(getalString.toString().equals(omgekeerdGetalString.toString())){
                System.out.println(i + " is het eerst volgende palindroom.");
                palindroomGevonden = true;
                break;
            }else{
                getalString.delete(0,Integer.MAX_VALUE);
                omgekeerdGetalString.delete(0,Integer.MAX_VALUE);
            }
        }
        if(!palindroomGevonden){
            System.out.println("Er zijn geen palindromen gevonden tussen " + getal1 + " en " + getal2);
        }
    }//einde eerste palindroom tussen twee waarden

    public static void maakPalindroom(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Geef een string in: ");
        String tekst = keyboard.nextLine();
        StringBuilder tekstReverse = new StringBuilder(tekst).reverse();
        StringBuilder optie1 = new StringBuilder(tekst);
        StringBuilder optie2 = new StringBuilder(tekst);

        optie1.append(tekstReverse);
        optie2.deleteCharAt(optie2.length()-1).append(tekstReverse);

        System.out.println("De opties zijn \"" + optie1 + "\" of \"" + optie2 + "\".");

    }//einde maak palindroom

    public static void toonMenu(){
        String[] opties = {
                "Toon de rij van Fibonacci",
                "Toon de som van even getallen van de rij van Fibonacci",
                "Is mijn getal een priemgetal?",
                "Wat is het eerst volgende priemgetal?",
                "Wat is het eerst volgende priemgetallen in het bereik van twee getallen?",
                "Is deze regel een palidroom?",
                "Is dit getal een palindroom?",
                "Wat is het eerst volgende palindroom? (getal)",
                "Wat is het eerst volgende palindroom in het bereid van twee getallen?",
                "Maak een palindroom",
                "Beëindig het programma"
        };

        for(int i = 0; i<opties.length; i++){
            System.out.println(i+1 + ": " + opties[i]);
        }

        System.out.print("Gelieve een keuze te maken (getal): ");
    }//einde toon menu
}
