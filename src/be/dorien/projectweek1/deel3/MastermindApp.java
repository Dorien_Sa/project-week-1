package be.dorien.projectweek1.deel3;

import java.util.Scanner;

public class MastermindApp {
    public static void main(String[] args){
        MastermindApp mastermindApp = new MastermindApp();
        Scanner keyboard = new Scanner(System.in);

        String answer = mastermindApp.makeAnswer();

        mastermindApp.startGame();

        String guess;
        String result;
        int numberOfGuesses = 1;

        do {
            System.out.print("Guess " + numberOfGuesses + " : ");

            try{
                guess = keyboard.nextLine();
                mastermindApp.wrongInput(guess);
            }catch(IllegalArgumentException iae){
                System.out.println(iae.getMessage());
                System.out.println("Please enter a valid guess.");
                guess = "Try again";
            }
            result = mastermindApp.check(guess, answer);
            System.out.println(result);
            numberOfGuesses++;

        } while (!result.equals("++++") || guess.equals("Try again"));

        System.out.println("You solved it!");
    }

    protected String check(String guess, String answer){
        String result = "";

        for(int i = 0; i<4 ; i++){
            if(guess.charAt(i) == answer.charAt(i)){
                guess = setStringX(guess, i);
                result += "+";
//                System.out.println(guess);
            }
        }

        for(int i = 0; i<4 ; i++){
            for(int j = 0; j<4 ; j++){
                if(guess.charAt(i) == answer.charAt(j)){
                    result += "-";
                }
            }
        }
//        System.out.println(guess);
        return result;
    }

    protected String makeAnswer(){
        String answer = "";
        for(int i = 0; i<4 ; i++){
            int newDigit = (int) (Math.random() * 6 + 1);
            if(i == 0){
                answer += newDigit;
            }else{
                for(int j = 0; j < answer.length() ; j++){
                    String newDigitString = String.valueOf(newDigit);
                    if(newDigitString.charAt(0) == answer.charAt(j)){
                        i--;
                        break;
                    }else if(newDigit != answer.charAt(j) && j == answer.length() -1){
                        answer += newDigit;
                        break;
                    }
                }
            }
        }
//        System.out.println(answer);
        return answer ;
    }

    protected String setStringX(String str, int xPosition){
        return str.replace(str.charAt(xPosition), 'x');
    }

    void startGame(){
        System.out.println("Welcome to Mastermind!");
        System.out.println("I'm thinking of a 4-digit code, with numbers starting from 1 up to 6.");
        System.out.println("Duplicate values are not allowed.");
        System.out.println("'+' represents a correct number guessed in the correct position.");
        System.out.println("'-' represents a correct number guessed, but in the wrong position.");
    }

    public void wrongInput(String guess){
        if(guess.length() != 4) throw new IllegalArgumentException("Your guess has to be 4 digits long.");
        for(int i = 0; i < guess.length(); i++){
            int character = Character.getNumericValue(guess.charAt(i));
            if(character < 1 || character > 6) throw new IllegalArgumentException("Your guess has to contain numbers from 1 to 6.");
        }
    }
}
